package onciu.net.appspatr.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import onciu.net.appspatr.R;

/**
 * Created by Cristian Nicolae Onciu on 14/09/16.
 */
public class PhotoView2 extends View {
    private final static String TAG = PhotoView2.class.getSimpleName();

    //the bitmap stored as a byte array
    private byte[] image;

    private int backgroundColor;
    private String labelText;

    //paint used for drawing
    private Paint labelPaint;
    private Paint imagePaint;

    public PhotoView2(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        //read xml attributes
        TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.photo_view2, 0, 0);
        int labelTextSize = ta.getDimensionPixelSize(R.styleable.photo_view2_labelTextSize, 12);
        int labelTextColor = ta.getColor(R.styleable.photo_view2_labelTextColor, Color.WHITE);
        backgroundColor = ta.getColor(R.styleable.photo_view2_backgroundColor, Color.BLACK);
        labelText = ta.getString(R.styleable.photo_view2_labelText);
        ta.recycle();

        labelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        labelPaint.setTextSize(labelTextSize);
        labelPaint.setColor(labelTextColor);
        labelPaint.setTextAlign(Paint.Align.CENTER);
        labelPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));


        imagePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        imagePaint.setColor(Color.BLACK);
    }

    /**
     * Calculates the position for label and draws it
     * The label will be horizontally center aligned
     *
     * @param canvas
     */
    private void drawLabel(Canvas canvas) {
        float x = getWidth()  / 2;
        //the y coordinate marks the bottom of the text, so we need to factor in the height
        Rect bounds = new Rect();
        labelPaint.getTextBounds(labelText, 0, labelText.length(), bounds);
        float y = (getHeight() - getPaddingBottom()) / 10;
        canvas.drawText(labelText, x, y, labelPaint);
    }

    /**
     * Scales the bitmap and draws it
     *
     * @param canvas
     */
    private void drawImage(Canvas canvas) {
        if (image != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
            int bmpHeight = bitmap.getHeight();
            int bmpWidth = bitmap.getWidth();
            float scale = Math.min(getWidth() / bmpWidth,getHeight() / bmpHeight);
            float xTranslation = (getWidth()  - bmpWidth * scale) / 2;
            float yTranslation = (getHeight()  - bmpHeight * scale) /2;
            Matrix transformation = new Matrix();
            transformation.postTranslate(xTranslation, yTranslation);
            transformation.preScale(scale, scale);
            imagePaint.setFilterBitmap(true);
            canvas.drawBitmap(bitmap, transformation, imagePaint);
            bitmap.recycle();
        }
    }

    /**
     * Just fills the canvas with the background color
     *
     * @param canvas
     */
    private void drawBackground(Canvas canvas) {
        canvas.drawColor(backgroundColor);
    }


    /**
     * Paints everything inside the canvas
     *
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        drawBackground(canvas);
        drawImage(canvas);
        drawLabel(canvas);
    }

    private int measureHeight(int measureSpec) {

        int size = getPaddingTop() + getPaddingBottom();
        size += labelPaint.getFontSpacing();

        if (image != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(image, 0, image.length, options);
            int imageHeight = options.outHeight;
            //Log.d(TAG,"imageH "+imageHeight);
            size += imageHeight;
        }


        return resolveSizeAndState(size, measureSpec, 0);
    }

    private int measureWidth(int measureSpec) {

        int size = getPaddingLeft() + getPaddingRight();
        Rect bounds = new Rect();
        labelPaint.getTextBounds(labelText, 0, labelText.length(), bounds);
        size += bounds.width();

        if (image != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(image, 0, image.length, options);
            int imageWidth = options.outWidth;
            //Log.d(TAG,"imageW "+imageWidth);
            size += imageWidth;
        }


        return resolveSizeAndState(size, measureSpec, 0);
    }

    /**
     * Measure the view and its content to determine the measured width and the measured height.
     *
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    public void setLabel(String label) {
        this.labelText = label;
        invalidate();
        requestLayout();
    }

    public void setImage(byte[] image) {
        this.image = image;
        invalidate();
        requestLayout();
    }
}
