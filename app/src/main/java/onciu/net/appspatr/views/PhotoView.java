package onciu.net.appspatr.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import onciu.net.appspatr.R;

/**
 * Created by Cristian Nicolae Onciu on 12/09/16.
 */
public class PhotoView extends LinearLayout {
    public final static String TAG = PhotoView.class.getSimpleName();

    private ImageView imageView;
    private TextView label;

    public PhotoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public PhotoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PhotoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initLayout(context);
    }

    private void initLayout(Context context) {
        setOrientation(VERTICAL);
        imageView = new ImageView(context);
        label = new TextView(context);
        setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
        LayoutParams imageParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        imageParams.setMargins(5, 5, 5, 5);
        imageParams.gravity = Gravity.CENTER;
        addView(imageView, imageParams);
        LayoutParams textParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        textParams.setMargins(10, 5, 10, 5);
        textParams.gravity = Gravity.CENTER_HORIZONTAL;
        addView(label, textParams);
    }
}
