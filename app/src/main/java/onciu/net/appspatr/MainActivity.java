package onciu.net.appspatr;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import onciu.net.appspatr.views.PhotoFragment;

/**
 * Challange 3 (Android)
 * Create a custom view (extend View or any of its subclasses) containing at least one image and a title and then display a few of those custom views in a RecyclerView along the y-axis.
 * Requirements / constraints:
 * No external dependencies (Exception: Google APIs)
 * MinSDKVersion (14)
 * Built with Android Studio
 * Bonus:
 * Make sure the app is handling all screen sizes
 * Get and display data from any API (examples: http://jsonplaceholder.typicode.com/)
 * Send your repository’s link along with your application
 */

/**
 * Created by Cristian Nicolae Onciu on 12/09/16.
 */

public class MainActivity extends AppCompatActivity implements PhotoFragment.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getFragmentManager();
        PhotoFragment photoFragment = (PhotoFragment) fragmentManager.findFragmentByTag(PhotoFragment.TAG);

        if (photoFragment == null) {
            photoFragment = new PhotoFragment();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.fragment_placeholder, photoFragment, PhotoFragment.TAG);
            transaction.commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
