package onciu.net.appspatr.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import onciu.net.appspatr.R;
import onciu.net.appspatr.model.TypicodePhoto;
import onciu.net.appspatr.net.AppspatrService;
import onciu.net.appspatr.views.PhotoView2;

/**
 * Created by Cristian Nicolae Onciu on 9/14/16.
 */
public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.Holder> {
    private static final String TAG = PhotoAdapter.class.getSimpleName();

    private TypicodePhoto[] tps;


    public PhotoAdapter(TypicodePhoto[] tps) {
        this.tps = tps;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View photoView = inflater.inflate(R.layout.photo_view, parent, false);
        Holder viewHolder = new Holder(photoView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        //Log.d(TAG, "binding view " + position);
        final TypicodePhoto tp = tps[position];
        if (holder.tp == null) {
            holder.update(tp);
        }
    }

    @Override
    public int getItemCount() {
        return tps.length;
    }

    static class Holder extends RecyclerView.ViewHolder {
        public PhotoView2 photoView;
        //public TextView textView;
        public TypicodePhoto tp;
        public byte[] image;

        public Holder(View itemView) {
            super(itemView);
            //photoView = (ImageView) itemView.findViewById(R.id.imageView);
            //textView = (TextView) itemView.findViewById(R.id.textView);
            photoView = (PhotoView2) itemView.findViewById(R.id.photo_view);
        }

        public void update(final TypicodePhoto tp) {
            this.tp = tp;
            //textView.setText(tp.getTitle());
            photoView.setLabel(tp.getTitle());
            ResultReceiver resultReceiver = new ResultReceiver(new Handler()) {
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    switch (resultCode) {
                        case AppspatrService.OK:
                            //Log.d(TAG, "tp retrieved successfully");
                            image = resultData.getByteArray(AppspatrService.RESULT);
                            //photoView.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
                            photoView.setImage(image);
                            break;
                        case AppspatrService.ERROR:
                            Log.e(TAG, "cannot retrieve image from " + tp.getUrl());
                            break;
                    }
                }
            };
            Intent intent = new Intent(itemView.getContext(), AppspatrService.class);
            intent.setAction(AppspatrService.ACTION_DOWNLOAD_IMAGE);
            intent.setData(Uri.parse(tp.getUrl().toString()));
            intent.putExtra(AppspatrService.RECEIVER, resultReceiver);
            itemView.getContext().startService(intent);
        }
    }
}
