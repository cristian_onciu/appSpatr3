package onciu.net.appspatr.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Cristian Nicolae Onciu on 9/13/16.
 */
public class TypicodePhoto implements Parcelable {
    private int albumId;
    private int id;
    private String title;
    private URL url;
    private URL thumbnailUrl;

    private final static String ALBUM_ID = "albumId";
    private final static String ID = "id";
    private final static String TITLE = "title";
    private final static String URL = "url";
    private final static String THUMB_URL = "thumbnailUrl";

    private TypicodePhoto(Builder builder) {
        albumId = builder.albumId;
        id = builder.id;
        title = builder.title;
        url = builder.url;
        thumbnailUrl = builder.thumbnailUrl;
    }

    public static final class Builder {
        private int albumId;
        private int id;
        private String title;
        private URL url;
        private URL thumbnailUrl;

        public Builder() {
        }

        public Builder albumId(int val) {
            albumId = val;
            return this;
        }

        public Builder id(int val) {
            id = val;
            return this;
        }

        public Builder title(String val) {
            title = val;
            return this;
        }

        public Builder url(URL val) {
            url = val;
            return this;
        }

        public Builder thumbnailUrl(URL val) {
            thumbnailUrl = val;
            return this;
        }

        public TypicodePhoto build() {
            return new TypicodePhoto(this);
        }
    }

    /*public TypicodePhoto(int albumId, int id, String title, URL url, URL thumbnailUrl) {
        this.albumId = albumId;
        this.id = id;
        this.title = title;
        this.url = url;
        this.thumbnailUrl = thumbnailUrl;
    }*/


    public int getAlbumId() {
        return albumId;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public URL getUrl() {
        return url;
    }

    public URL getThumbnailUrl() {
        return thumbnailUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypicodePhoto that = (TypicodePhoto) o;

        if (albumId != that.albumId) return false;
        if (id != that.id) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        return thumbnailUrl != null ? thumbnailUrl.equals(that.thumbnailUrl) : that.thumbnailUrl == null;

    }

    @Override
    public int hashCode() {
        int result = albumId;
        result = 31 * result + id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (thumbnailUrl != null ? thumbnailUrl.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.albumId);
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeSerializable(this.url);
        dest.writeSerializable(this.thumbnailUrl);
    }

    protected TypicodePhoto(Parcel in) {
        this.albumId = in.readInt();
        this.id = in.readInt();
        this.title = in.readString();
        this.url = (URL) in.readSerializable();
        this.thumbnailUrl = (URL) in.readSerializable();
    }

    public static final Creator<TypicodePhoto> CREATOR = new Creator<TypicodePhoto>() {
        @Override
        public TypicodePhoto createFromParcel(Parcel source) {
            return new TypicodePhoto(source);
        }

        @Override
        public TypicodePhoto[] newArray(int size) {
            return new TypicodePhoto[size];
        }
    };

    @Override
    public String toString() {
        return "TypicodePhoto[" +
                "albumId:" + albumId +
                ", id:" + id +
                ", title:'" + title + '\'' +
                ", url:" + url +
                ", thumbnailUrl:" + thumbnailUrl +
                ']';
    }

    public static TypicodePhoto[] createFromJson(String arrayOfTypicodeAsString) throws JSONException {
        JSONArray jsonArray = new JSONArray(arrayOfTypicodeAsString);
        int numberOfPhotos = jsonArray.length();
        TypicodePhoto[] tps = new TypicodePhoto[numberOfPhotos];
        for (int i = 0; i < numberOfPhotos; i++) {
            JSONObject json = jsonArray.getJSONObject(i);
            try {
                TypicodePhoto tp = new Builder()
                        .albumId(json.getInt(ALBUM_ID))
                        .id(json.getInt(ID))
                        .title(json.getString(TITLE))
                        .url(new URI(json.getString(URL)).toURL())
                        .thumbnailUrl(new URI(json.getString(THUMB_URL)).toURL())
                        .build();
                tps[i] = tp;
                //Log.d(TypicodePhoto.class.getSimpleName(), tp.toString());
            } catch (MalformedURLException | URISyntaxException e) {
                Log.e(TypicodePhoto.class.getSimpleName(), e.getMessage(), e.getCause());
            }
        }
        return tps;
    }
}
